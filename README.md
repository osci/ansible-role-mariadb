Ansible module used by OSAS to enable mariadb (or MySQL if too old)

[![Build Status](https://travis-ci.org/OSAS/ansible-role-mariadb.svg?branch=master)](https://travis-ci.org/OSAS/ansible-role-mariadb)

## Usage

To read parameters documentation, use this command:

```
ansible-doc -t role mariadb

```
